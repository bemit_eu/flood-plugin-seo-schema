<?php

namespace FloodPluginSeoSchema;

class Content {
    public static function recipe($data) {
        $schema = [
            '@context' => 'http://schema.org',
            '@type' => 'Recipe',
        ];

        if(isset($data['name']) && !empty($data['name'])) {
            $schema['name'] = $data['name'];
        }
        if(isset($data['author']) && !empty($data['author'])) {
            $schema['author'] = $data['author'];
        }
        if(isset($data['datePublished']) && !empty($data['datePublished'])) {
            $schema['datePublished'] = $data['datePublished'];
        }
        if(isset($data['description']) && !empty($data['description'])) {
            $schema['description'] = $data['description'];
        }
        if(isset($data['image']) && !empty($data['image'])) {
            $schema['image'] = $data['image'];
        }
        if(isset($data['recipeInstructions']) && !empty($data['recipeInstructions'])) {
            $schema['recipeInstructions'] = $data['recipeInstructions'];
        }
        if(isset($data['suitableForDiet']) && !empty($data['suitableForDiet'])) {
            $schema['suitableForDiet'] = 'http://schema.org/' . $data['suitableForDiet'];
        }
        if(isset($data['recipeIngredient']) && !empty($data['recipeIngredient'])) {
            $schema['recipeIngredient'] = $data['recipeIngredient'];
        }
        if(isset($data['prepTime']) && !empty($data['prepTime'])) {
            $schema['prepTime'] = 'PT ' . $data['prepTime'] . 'M';
        }
        if(isset($data['cookTime']) && !empty($data['cookTime'])) {
            $schema['cookTime'] = 'PT ' . $data['cookTime'] . 'M';
        }
        if(isset($data['recipeYield']) && !empty($data['recipeYield'])) {
            $schema['recipeYield'] = $data['recipeYield'];
        }

        return $schema;
    }
}
