<?php

class FloodPluginSeoSchema {
    public static $fns;

    public static function init() {
        static::$fns['schemaRecipe'] = [\FloodPluginSeoSchema\Content::class, 'recipe'];
    }

    /**
     * @param $template_engine \HydroFeature\Template\Template
     *
     * @todo add flood/canal compatibility
     */
    public static function register($template_engine) {
        $template_engine->addPath('vendor/flood-plugin/seo-schema/view', 'seo-schema');

        static::init();

        foreach(static::$fns as $name => $fn) {
            $template_engine->addFunction($name, $fn);
        }
    }
}
